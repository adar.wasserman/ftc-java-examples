package threads;

public class ThreadExample extends Thread{

    public volatile String variable;

    public ThreadExample(String _variable){
        this.variable = _variable;
    }

    @Override
    public void run()
    {
        System.out.println("Running in a separate thread!");
        System.out.println(variable);
        while (!isInterrupted());
        System.out.println("Hello");
    }

}

