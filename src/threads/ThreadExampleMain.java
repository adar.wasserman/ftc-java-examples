package threads;

public class ThreadExampleMain {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new ThreadExample("Hi!");
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }

}
