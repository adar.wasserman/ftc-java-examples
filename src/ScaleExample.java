public class ScaleExample {

    public static double scale(double n, double x1, double x2, double y1, double y2) {
        double a = (y1-y2)/(x1-x2);
        double b = y1 - x1*(y1-y2)/(x1-x2);
        return a*n+b;
    }

    public static void main(String[] args) {
        System.out.println(scale(5, 0, 1200, 0, 63453));
    }
}