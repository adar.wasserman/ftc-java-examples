public class WhileExample {
    public static void main(String[] args) {
        int index = 0;
        while(index != 5){
            System.out.println(index);
            index++; //can also be index = index + 1; or index += 1;
        }
    }
}
