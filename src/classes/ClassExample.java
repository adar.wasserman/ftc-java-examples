package classes;

public class ClassExample {

    public static void main(String[] args) {
        Student student = new Student();
        student.name = "John Doe";
        student.age = 45;
        student.identifier = "00000000000";

        while(true){
            for (int i = 0; i < 20; i++) {
                student.programTheRobot();
            }
            student.eat();
            student.sleep();
        }
    }

}