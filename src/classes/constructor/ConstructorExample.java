package classes.constructor;


public class ConstructorExample {

    public static void main(String[] args) {
        ConstructorStudent student = new ConstructorStudent("John Doe", 45,"00000000000");

        while(true){
            for (int i = 0; i < 20; i++) {
                student.programTheRobot();
            }
            student.eat();
            student.sleep();
        }
    }

}

