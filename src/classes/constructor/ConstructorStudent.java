package classes.constructor;

public class ConstructorStudent {

    public String name;
    public int age;
    public String identifier;

    public ConstructorStudent(String _name, int _age, String _identifier){
        name = _name;
        age = _age;
        identifier = _identifier;
    }

    public void eat(){
        System.out.println(name + " is eating!");
    }

    public void sleep(){
        System.out.println(name + " is sleeping!");
    }

    public void programTheRobot(){
        System.out.println(name + " is programming the robot!");
    }

}
