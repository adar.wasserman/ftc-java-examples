package classes;

public class Student {

    public String name;
    public int age;
    public String identifier;

    public void eat(){
        System.out.println(name + " is eating!");
    }

    public void sleep(){
        System.out.println(name + " is sleeping!");
    }

    public void programTheRobot(){
        System.out.println(name + " is programming the robot!");
    }

}
