public class FunctionExample {

    public static void main(String[] args) {
        System.out.println(returnName());
        print("Hello!");
    }

    public static String returnName(){
        return "John Doe";
    }

    public static void print(String whatToPrint){
        System.out.println(whatToPrint);
    }

}
